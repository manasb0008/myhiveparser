# README #
Open any java editor and run myHiveParser3Helper.java

### What is this repository for? ###

* This is for comparing if two Hive queries are similar.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Simple java environment is enough to setup
* Look pom.xml for any queries
* Dependencies are taken care by maven
* Database configuration
* Run tests through myHiveParser3.java
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin manasb0008
* supervised by devjyotip