
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manasb
 */
import com.qubole.manas.myHiveParser3;


public class myHiveParser3Helper {


    public static void main(String args[]) throws Exception{


      myHiveParser3 parser = new myHiveParser3();

      String query = "SELECT s.account_id AS account_id,\n" +
              "        s.db_name AS db_name,\n" +
              "        s.table_name AS table_name,\n" +
              "        s.column_name,\n" +
              "        s.user_id AS user_id,\n" +
              "        rank() OVER (PARTITION BY s.account_id ORDER BY s.usedcount desc) AS field_rank,\n" +
              "        \"NA\" AS usage\n" +
              "        FROM (SELECT u.account_id AS account_id,\n" +
              "            tbl,\n" +
              "            LOWER( split ( u.tbl ,'[\\.]' ) [0] ) AS db_name,\n" +
              "            LOWER( split ( u.tbl ,'[\\.]' ) [1] ) AS table_name,\n" +
              "            LOWER(u.col) AS column_name,\n" +
              "            t.user_id AS user_id,\n" +
              "            COUNT(DISTINCT u.query_hists_id) AS usedcount\n" +
              "            FROM usagemap u JOIN rstore.query_hists h\n" +
              "            ON  u.account_id = h.account_id\n" +
              "                AND u.query_hists_id = h.id\n" +
              "            JOIN rstore.qbol_users t\n" +
              "\t\t\t  ON h.qbol_user_id = t.id\n" +
              "            WHERE u.account_id IN (11, 6, 1143, 1208, 7, 9, 13, 1201, 30, 1195, 101, 1203)\n" +
              "                AND u.submit_time > '2017-03-01'\n" +
              "            GROUP BY u.account_id, t.user_id, tbl, u.col) s\n" +
              "        ORDER BY field_rank LIMIT 100";



      String query2 = "SELECT s.account_id AS account_id,\n" +
              "        s.db_name AS db_name,\n" +
              "        s.table_name AS table_name,\n" +
              "        s.column_name,\n" +
              "        s.user_id AS user_id,\n" +
              "        rank() OVER (PARTITION BY s.account_id ORDER BY s.usedcount desc) AS field_rank,\n" +
              "        \"NA\" AS usage\n" +
              "        FROM (SELECT u.account_id AS account_id,\n" +
              "            tbl,\n" +
              "            LOWER( split ( u.tbl ,'[\\.]' ) [0] ) AS db_name,\n" +
              "            LOWER( split ( u.tbl ,'[\\.]' ) [1] ) AS table_name,\n" +
              "            LOWER(u.col) AS column_name,\n" +
              "            t.user_id AS user_id,\n" +
              "            COUNT(DISTINCT u.query_hists_id) AS usedcount\n" +
              "            FROM usagemap u JOIN rstore.query_hists h\n" +
              "            ON  u.account_id = h.account_id\n" +
              "                AND u.query_hists_id = h.id\n" +
              "            JOIN rstore.qbol_users t\n" +
              "\t\t\t  ON h.qbol_user_id = t.id\n" +
              "            WHERE u.account_id IN (1,2,3,4,5,6,7,8,9,10)\n" +
              "                AND u.submit_time > '2017-03-23'\n" +
              "            GROUP BY u.account_id, t.user_id, tbl, u.col) s\n" +
              "        ORDER BY field_rank LIMIT 200";
        
      System.out.println("\n\n\n"+parser.parse(query,query2));
     
  }
}
