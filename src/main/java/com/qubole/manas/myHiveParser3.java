package com.qubole.manas;

/**
 * Created by manasb on 22/5/17.
 */

import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.ql.Context;
import org.apache.hadoop.hive.ql.parse.ASTNode;
import org.apache.hadoop.hive.ql.parse.HiveParser;
import org.apache.hadoop.hive.ql.parse.ParseDriver;
import static org.apache.hadoop.hive.ql.parse.HiveParser.TOK_NULL;


public class myHiveParser3 {

    private ParseDriver parseDriver = new ParseDriver();
    static int level=1;
    

    public boolean parse(String query,String query2) throws Exception{
    
        HiveConf conf = new HiveConf();

        String scratchDir = HiveConf.getVar(conf, HiveConf.ConfVars.SCRATCHDIR);
        conf.setBoolVar(HiveConf.ConfVars.HIVE_SUPPORT_SQL11_RESERVED_KEYWORDS, false);
        conf.set("_hive.hdfs.session.path", scratchDir);
        conf.set("_hive.local.session.path", HiveConf.getVar(conf, HiveConf.ConfVars.LOCALSCRATCHDIR)
                + "/" + System.getProperty("user.name") + "/" + "000");

        ASTNode astNode = parseDriver.parse(query, new Context(conf));
        ASTNode astNode2 = parseDriver.parse(query2, new Context(conf));


        System.out.println("AST of query 1 :\n"+((ASTNode)(astNode.getChild(0))).dump());
        System.out.println("AST of query 2 :\n"+((ASTNode)(astNode2.getChild(0))).dump());


        return parseNode((ASTNode)astNode.getChild(0),(ASTNode)astNode2.getChild(0),level);

    }

    public static boolean parseNode(ASTNode node,ASTNode node2,int level) throws Exception{
        
         

        if(isHaltingCondnsSatisfied(node,node2))
        {
            return handleCondns(node,node2);
        }


        int numofchildren = node.getChildren().size();

        
        boolean result = true;
        
        System.out.println("------------------level <"+level+">--------------------");


        for(int i=0;i<numofchildren;++i){
            
            ASTNode ast = (ASTNode)node.getChild(i);
            ASTNode ast2 = (ASTNode)node2.getChild(i);


            //if the keyword "IN" is found - donot traverse the list since it has nothing but literals
            if(i>1 && ((ASTNode)node.getChild(i-2)).getToken().getType()==HiveParser.KW_IN)
            {
                System.out.println("'in' condn satisfied");
                break;
            }

            int tokentype = ast.getToken().getType();
            int tokentype2 = ast2.getToken().getType();


            //if the node is a literal - do not compare
            if(isLiteral(tokentype) && isLiteral(tokentype2))
                continue;

            String token = ast.toString();
            String token2 = ast2.toString();
            

            //if the tokens are found different (except the aliases) return false)
            //Aliases can be different.They are identified through their parent nodes which can be tok_selexpr or tok_tabref
            if(token.compareTo(token2)!=0 && ( ((ASTNode)ast2.getParent()).getToken().getType()!=HiveParser.TOK_SELEXPR &&
                ((ASTNode)ast2.getParent()).getToken().getType()!=HiveParser.TOK_TABREF) )
            {
                System.out.println("******************************************");
                System.out.println(token +"-" + token2 + " are found different");
                System.out.println("******************************************");
                return false;
            }

            //print the tokens traversed
            System.out.println(token + "-" + token2);
            
            
        }

        //traverse the ast recursively
        for(int i=0;i<numofchildren;++i){

            //if the keyword "IN" is found - donot traverse the list since it has nothing but literals
            if(i>1 && ((ASTNode)node.getChild(i-2)).getToken().getType()==HiveParser.KW_IN)
            {
                return result;
            }
            result = result & parseNode((ASTNode)node.getChild(i),(ASTNode)node2.getChild(i),level+1);

        }

        
        return result;
        

    }



    public static boolean isLiteral(int operator) {
    return (operator == HiveParser.Number)
        || (operator == HiveParser.StringLiteral)
        || (operator == HiveParser.BigintLiteral)
        || (operator == HiveParser.TinyintLiteral)
        || (operator == HiveParser.SmallintLiteral)
        || (operator == HiveParser.KW_TRUE)
        || (operator == HiveParser.KW_FALSE)
        || (operator == TOK_NULL);
  }

  public static boolean isHaltingCondnsSatisfied(ASTNode node,ASTNode node2){
      if(node==null && node2==null)
          return true;
      if(node==null || node2==null)
          return true;
      if(node.getChildren()==null && node2.getChildren()==null)
          return true;
      if(node.getChildren()==null || node2.getChildren()==null)
          return true;
      return false;
  }


    public static boolean handleCondns(ASTNode node,ASTNode node2){
        if(node==null && node2==null)
            return true;
        if(node==null || node2==null)
            return false;
        if(node.getChildren()==null && node2.getChildren()==null)
            return true;
        if(node.getChildren()==null || node2.getChildren()==null)
            return false;
        return false;
    }

}
